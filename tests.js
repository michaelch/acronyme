var expect = require('chai').expect;
var Anagram = require("./anagram");

describe('Anagram', function(){
    describe('#getOriginal', function(){

        it('should keep an the original sentence', function(){
            var anagram = Anagram('Ceci est une phrase que nous allons transformer en anagram.');
            expect(anagram.getOriginal()).equal('Ceci est une phrase que nous allons transformer en anagram.');
        });

    });
    
    describe('#cleanOriginal', function(){

        it('should keep letters', function(){
            var anagram = Anagram("Je m'appelle Michael.");
            expect(anagram.cleanOriginal()).to.equal("Je m appelle Michael");
        });

        it('should keep numbers', function(){
            var anagram = Anagram("J'ai 26 ans!'");
            expect(anagram.cleanOriginal()).to.equal("J ai 26 ans");
        });

        it('should keep symbols', function(){
            var anagram = Anagram("J'ai 26 ans!'");
            expect(anagram.cleanOriginal()).to.equal("J ai 26 ans");
        });

        it('should not keep punctuations', function(){
            var anagram = Anagram("!!!!!!");
            expect(anagram.cleanOriginal()).to.equal("");
        });


        it('should only keep letters, numbers or money symbols ($, €, £) (french notation)', function(){
            var anagram = Anagram("J'ai 5$ dans ma poche-droite");
            expect(anagram.cleanOriginal()).to.equal("J ai 5$ dans ma poche droite");
        });


        
        it('should only keep letters, numbers or money symbols ($, €, £) (american notation)', function(){
            var anagram = Anagram("J'ai $5 dans ma poche-droite");
            expect(anagram.cleanOriginal()).to.equal("J ai $5 dans ma poche droite");
        });

        it('should keep a maximum space of one between each word', function(){
            var anagram = Anagram("J'ai mis  un espace en  trop?");
            expect(anagram.cleanOriginal()).to.equal("J ai mis un espace en trop");
        });



    });
    
    describe('#getFirstLetter', function(){

        it('should get the first letter of each words', function(){
            var anagram = Anagram("Je m'appelle Michael.");
            expect(anagram.getFirstLetter()).to.deep.equal(["J", "M", "A", "M"]);
        });

        it('should keep all numbers as one letter', function(){
            var anagram = Anagram("J'ai 5000  dollar dans ma poche-droite");
            expect(anagram.getFirstLetter()).to.deep.equal(["J", "A", "5000", "D", "D", "M", "P", "D"]);
        });

        it('should keep numbers and symbols where they are together (french notation)', function(){
            var anagram = Anagram("J'ai 5000$ dans ma poche-droite");
            expect(anagram.getFirstLetter()).to.deep.equal(["J", "A", "5000$", "D", "M", "P", "D"]);
        });

        it('should keep numbers and symbols when they are together (american notation)', function(){
            var anagram = Anagram("J'ai $5000 dans ma poche-droite");
            expect(anagram.getFirstLetter()).to.deep.equal(["J", "A", "$5000", "D", "M", "P", "D"]);
        });
        
    });
});