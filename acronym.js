/**
* Created with Anagram.
* User: infogenious
* Date: 2015-01-05
* Time: 09:41 AM
* To change this template use Tools | Templates.
*/

function Anagram(sentence) {
    
    if  (!(this instanceof Anagram)) {
        return new Anagram(sentence);
    }

    var original = sentence;
    
    this.getOriginal = function() {
        return original;
    };
    
    return this;
    
}

Anagram.prototype.cleanOriginal = function() {
    var XRegExp = require('xregexp').XRegExp;

    var cleaner = XRegExp('[^\\w\\p{Sc}]', 'g');
    return XRegExp.replace(this.getOriginal(), cleaner, ' ').replace(/\s{2,}/g, ' ').trim();
};

Anagram.prototype.getFirstLetter  = function() {
    var XRegExp = require('xregexp').XRegExp;
    var currency = XRegExp('\\p{N}+\\p{Sc}|\\p{Sc}\\p{N}+');
    
    var cleaned = this.cleanOriginal().split(' ').map(function firstLetter(e){
        if (e === (+e).toFixed()) {
            return e;
        }
        if (XRegExp.test(e, currency)) {
            return e;
        }
        return e[0].toUpperCase();
    });
    return cleaned;
};

module.exports = Anagram;